# python_tutorial_SRPI

singing reading person identification 

# project title 

## (S)singing and (R)reading (P)person (I)identification

## preview of the product

![pic](http://p1.bpimg.com/567571/34673bd2ebe02de6.png)

## the dataset is processed in file "IUOUI_16000_mono_vad" 

* it contains 28 singing voice and reading voice 
 they both in form of mono channel and 16kHz with voice advicty detection by vad.py.
 
## all_model_predict_the_audio.py

load all the model and test the acc, vote the final predict name.

## countDays.py 

it is a small test program , count the days between two dates.
 
## crfTutorial.py

it is the usage of the crf example

## genMFCC.py mfcc.py  

use these pyfile generate the MFCC features

## getXy.py

process audio data to the format as the model need X and y.

# mainUI.py

it's the main function for user

## plotWav.py

use it plot the audio wav figure

## processAudioData.py

use it preprocess the audio to uniform data format.

## smallTest.py 

just like its name, it contains small test about python program.

# testCRF.py  testDT.py testGM.py testSGD.py testSVM.py 

these the five model we try on our project. and finally we choose the testDT.py as the final model.
and the result of crf is more than 61% ,the sgd is 16%
,GM and SVM are need more time for runing. the choosed DT achieves 100%.
 
# vad.py

voice activity detection for delete the silence frame.


@ author 

[ZHANG Xu-long](http://www.zhangxulong.site) 




 
 
