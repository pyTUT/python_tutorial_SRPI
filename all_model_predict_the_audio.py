import joblib
import scipy.io.wavfile as wav

from getXy import gen_dic_list_full_piece, calcMFCC
from testSGD import translate_the_result, predict_names_vote_one_name, get_owner_dict, process_data_sgd_single

modle_crf = 'model/crf.model'
crf = joblib.load(modle_crf)
modle_dt = 'model/dt.model'
dt = joblib.load(modle_dt)
modle_sgd = 'model/sgd.model'
sgd = joblib.load(modle_sgd)
modle_svm = 'model/svm.model'
sm = joblib.load(modle_svm)
modle_gm = 'model/gm.model'
gm = joblib.load(modle_gm)

# process data mfcc
def process_data(file_path):
    X = y = []
    (rate, sig) = wav.read(file_path)
    mfcc_feat = calcMFCC(sig, rate)
    owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
    mfcc_feat_dict = gen_dic_list_full_piece(mfcc_feat)
    X.append(mfcc_feat_dict)
    y.append(owner)
    return X, y



def all_model_predict(wav_path='IUOUI_16000_mono_vad/reading5-m-ck.wav'):
    process_X, process_y = process_data(wav_path)
    predict_names = []
    # load crf predict
    predict_y = crf.predict(process_X)
    predict_names = predict_y[0]
    name_singer_speaker = predict_names_vote_one_name(predict_names)
    print 'crf', name_singer_speaker
    predict_names.append(name_singer_speaker)
    # process new format data mfcc
    owner_dict = get_owner_dict('IUOUI_16000_mono_vad')
    process_X, process_y = process_data_sgd_single(wav_path, owner_dict)
    predict_y = dt.predict(process_X)
    name_singer_speaker = translate_the_result(predict_names_vote_one_name(predict_y), owner_dict)
    # name_singer_speaker = 'ck'
    print 'dt', name_singer_speaker
    predict_names.append(name_singer_speaker)
    predict_y = sgd.predict(process_X)
    name_singer_speaker = translate_the_result(predict_names_vote_one_name(predict_y), owner_dict)
    # name_singer_speaker = 'ck'
    print 'sgd', name_singer_speaker
    predict_names.append(name_singer_speaker)
    predict_y = sm.predict(process_X)
    name_singer_speaker = translate_the_result(predict_names_vote_one_name(predict_y), owner_dict)
    # name_singer_speaker = 'ck'
    print 'svm', name_singer_speaker
    predict_names.append(name_singer_speaker)
    predict_y = gm.predict(process_X)
    name_singer_speaker = translate_the_result(predict_names_vote_one_name(predict_y), owner_dict)
    # name_singer_speaker = 'ck'
    print 'gm', name_singer_speaker
    predict_names.append(name_singer_speaker)
    final_name = vote_final_name(predict_names)
    return final_name


# all_model_predict()

def vote_final_name(predict_list=['crf', 'dt','sgd','svm','gm' ]):
    final_name = ''
    # print predict_names

    acc_crf = 0.51
    acc_dt = 1.0
    acc_sgd = 0.17
    acc_svm=0.32
    acc_gm=0.68
    all_acc = [ acc_crf, acc_dt,acc_sgd,acc_svm,acc_gm]
    predict_names = []
    for item in range(len(predict_list)):
        predict_names.extend(int(all_acc[item] * 100) * [predict_list[item]])
    myset = set(list(predict_names))
    mydict = {}
    for item in myset:
        # print("the %d has found %d" % (item, predict_names.count(item)))
        mydict[item] = list(predict_names).count(item)
    max_time = sorted(mydict.values())[-1]

    for name, name_votes in mydict.iteritems():
        if name_votes == max_time:
            final_name = name
    return final_name


print vote_final_name()
