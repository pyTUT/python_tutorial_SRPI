# coding=utf-8
import os
import scipy.io.wavfile as wav

from genMFCC import *


def gen_dic_from_row(row=[8.0, 10, 12]):
    dict_row = {}
    for row_item in range(len(row)):
        dict_row[str(row_item + 1)] = row[row_item]
    return dict_row


def gen_dic_list_full_piece(mfcc_feat):
    mfcc_feat_dic_list = []
    # owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
    for row in range(len(mfcc_feat)):
        # print row
        dict_row = gen_dic_from_row(mfcc_feat[row])
        mfcc_feat_dic_list.append(dict_row)
    return mfcc_feat_dic_list


def gen_X_y_list(path='IUOUI_8k_mono'):
    X = []
    y = []
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename:
                file_path = os.path.join(parent, filename)
                (rate, sig) = wav.read(file_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
                mfcc_feat_dict=gen_dic_list_full_piece(mfcc_feat)
                # print owner
                # print(mfcc_feat.shape)
                # print mfcc_feat
                X.append(mfcc_feat_dict)
                y.append(owner)
    return X, y


def genSinging_X_y_list(path='IUOUI_8k_mono'):
    X = []
    y = []
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and 'reading5' not in filename:
                file_path = os.path.join(parent, filename)
                (rate, sig) = wav.read(file_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
                mfcc_feat_dict = gen_dic_list_full_piece(mfcc_feat)
                # print owner
                # print(mfcc_feat.shape)
                # print mfcc_feat
                X.append(mfcc_feat_dict)
                y.append(owner)
    return X, y


def genReading_X_y_list(path='IUOUI_8k_mono'):
    X = []
    y = []
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and 'singing5' not in filename:
                file_path = os.path.join(parent, filename)
                (rate, sig) = wav.read(file_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
                mfcc_feat_dict = gen_dic_list_full_piece(mfcc_feat)
                # print owner
                # print(mfcc_feat.shape)
                # print mfcc_feat
                X.append(mfcc_feat_dict)
                y.append(owner)
    return X, y

# X,y=gen_X_y_list()
# print len(X),len(y),len(X[0]),len(y[0])
