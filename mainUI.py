# coding=utf-8
import os
import tkFileDialog
from Tkinter import *  # 导入 Tkinter 库

import joblib
import scipy.io.wavfile as wav

from genMFCC import *
from getXy import gen_dic_list_full_piece
from testSGD import translate_the_result, get_owner_dict, predict_names_vote_one_name


def process_data(file_path):
    X = y = []
    (rate, sig) = wav.read(file_path)
    mfcc_feat = calcMFCC(sig, rate)
    owner = len(mfcc_feat) * [file_path.split('-')[-1][:-4]]
    mfcc_feat_dict = gen_dic_list_full_piece(mfcc_feat)
    X.append(mfcc_feat_dict)
    y.append(owner)
    return X, y


def select_file():
    filename = tkFileDialog.askopenfilename()
    # print filename
    entry_text_var.set(filename)
    entry_name_var.set(filename.split('-')[-1][:-4])
    return filename


def submit4predict():
    modle_crf = 'model/crf.model'
    crf = joblib.load(modle_crf)
    wav_path = entry_text_var.get()
    # print wav_path
    # process data mfcc
    process_X, process_y = process_data(wav_path)
    # load crf predict
    predict_y = crf.predict(process_X)
    predict_names = predict_y[0]
    name_singer_speaker = predict_names_vote_one_name(predict_names)
    # name_singer_speaker = 'ck'
    result_crf_show_label_var.set(name_singer_speaker)
    return name_singer_speaker

def submit4_dt_predict():
    owner_dict = get_owner_dict('IUOUI_16000_mono_vad')
    modle_crf = 'model/dt.model'
    crf = joblib.load(modle_crf)
    wav_path = entry_text_var.get()
    owner = wav_path.split('-')[-1][:-4]
    # process data mfcc
    X = []
    y = []
    (rate, sig) = wav.read(wav_path)
    mfcc_feat = calcMFCC(sig, rate)
    owner_label = owner_dict[owner] - 1
    for mfcc_vector in mfcc_feat:
        X.append(mfcc_vector)
        y.append(owner_label)
    # load crf predict
    predict_names = crf.predict(X)
    # print predict_names

    name_singer_speaker = predict_names_vote_one_name(predict_names)
    name_singer_speaker = translate_the_result(name_singer_speaker, owner_dict)
    # name_singer_speaker = 'ck'
    result_crf_show_label_var.set(name_singer_speaker)
    return name_singer_speaker


def total4predict_accuracy(path='IUOUI_16000_mono_vad'):
    accuracy = 0
    total = 0
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename:
                wav_path = os.path.join(parent, filename)
                owner = wav_path.split('-')[-1][:-4]
                # print wav_path
                total += 1.0
                # process data mfcc
                process_X, process_y = process_data(wav_path)
                # load crf predict
                crf = joblib.load('crf.model')
                predict_y = crf.predict(process_X)
                predict_names = predict_y[0]
                name_singer_speaker = predict_names_vote_one_name(predict_names)

                if name_singer_speaker == owner:
                    accuracy += 1.0
                else:
                    print 'error with ', wav_path
                    print name_singer_speaker
    acc = accuracy / total
    print  'total acc:', acc, '%', ' &recognize right:', accuracy, ',& total:', total

    return acc


root = Tk()  # 创建窗口对象的背景色
top_frame = Frame(root)
title_var = StringVar()
title_label = Label(top_frame, textvariable=title_var, background='green')
title_var.set('who is speaking or singing?')
title_label.pack()
top_frame.pack()
middle_frame = Frame(root)
entry_text_var = StringVar()
entry_text = Entry(middle_frame, textvariable=entry_text_var, state='disabled')
entry_text.pack(side=LEFT)
select_button = Button(middle_frame, text='Select', command=select_file)
select_button.pack(side=LEFT)
entry_name_var = StringVar()
entry_name = Entry(middle_frame, textvariable=entry_name_var, state='disabled')
entry_name.pack(side=LEFT)
submit_button = Button(middle_frame, text='Submit', command=submit4_dt_predict)
submit_button.pack()
middle_frame.pack()
bottom_frame = Frame(root)
result_crf_lable = Label(bottom_frame, text='Predict Result:')
result_crf_lable.pack(side=LEFT)
result_crf_show_label_var = StringVar()
result_crf_show_label = Label(bottom_frame, textvariable=result_crf_show_label_var, background='red', width=5)
result_crf_show_label.pack(side=LEFT)

bottom_frame.pack()
# total acc: 0.517857142857 %  &recognize right: 29.0 ,& total: 56.0
# total4predict_accuracy()# 统计总的识别率
root.mainloop()  # 进入消息循环
