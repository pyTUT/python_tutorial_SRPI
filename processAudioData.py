from pydub import AudioSegment
import os
path_to_process='IUOUI'
for parent, dirnames, filenames in os.walk('IUOUI'):
    for filename in filenames:
        if '.DS_Store' not in filename and '.doc' not in filename:
            file_path = os.path.join(parent, filename)
            rate=16000
            out_dir=path_to_process+'_'+str(rate)+'_mono'

            print 'processing... ' + file_path
            file_fromat=file_path[-3:]
            out_path = out_dir + file_path[5:-3]+'wav'
            sound = AudioSegment.from_file(file_path,file_fromat)
            new = sound.set_channels(1)
            new = new.set_frame_rate(rate)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            new.export(out_path, 'wav')
