import joblib
import sklearn_crfsuite
from sklearn_crfsuite import metrics

from getXy import genSinging_X_y_list, genReading_X_y_list, gen_X_y_list


def main():
    X_train, y_train = genReading_X_y_list('IUOUI_16000_mono_vad')
    X_test, y_test = genSinging_X_y_list('IUOUI_16000_mono_vad')

    crf = sklearn_crfsuite.CRF(
        algorithm='lbfgs',
        c1=0.1,
        c2=0.1,
        max_iterations=1000,
        all_possible_transitions=True
    )
    # print y_train[0]
    # print X_train[0]
    crf.fit(X_train, y_train)
    # print y_train[0]
    # save crf model
    joblib.dump(crf, 'model/crf.model')
    labels = list(crf.classes_)

    y_pred = crf.predict(X_test)
    metrics.flat_f1_score(y_test, y_pred,
                          average='weighted', labels=labels)

    labels = list(crf.classes_)

    y_pred = crf.predict(X_test)
    # TODO vote for a piece of audio
    metrics.flat_f1_score(y_test, y_pred,
                          average='weighted', labels=labels)

    # group B and I results
    sorted_labels = sorted(
        labels,
        key=lambda name: (name[1:], name[0])
    )
    print(metrics.flat_classification_report(
        y_test, y_pred, labels=sorted_labels, digits=3
    ))
    return 0

# main()
