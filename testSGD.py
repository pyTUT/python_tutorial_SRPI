# coding=utf-8
import os
import numpy as np
import joblib
from sklearn.linear_model import SGDClassifier
import scipy.io.wavfile as wav
from sklearn.multiclass import OneVsRestClassifier

from genMFCC import calcMFCC


def predict_names_vote_one_name(predict_names=[1, 1, 1, 1, 4, 4, 4]):
    vote_name = ''
    # print predict_names
    myset = set(list(predict_names))  # myset是另外一个列表，里面的内容是predict_names里面的无重复项
    mydict = {}
    for item in myset:
        # print("the %d has found %d" % (item, predict_names.count(item)))
        mydict[item] = list(predict_names).count(item)
    max_time = sorted(mydict.values())[-1]

    for name, name_votes in mydict.iteritems():
        if name_votes == max_time:
            vote_name = name
    return vote_name


def process_data_sgd(data_path):
    X = []
    y = []
    dict_value = 0
    owner_list = []
    owner_dict = {}
    for parent, dirnames, filenames in os.walk(data_path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and'singing'not in filename:
                file_path = os.path.join(parent, filename)
                owner = file_path.split('-')[-1][:-4]
                owner_list.append(owner)
    # print owner_list
    owner_set = set(owner_list)
    for owner_item in owner_set:
        owner_dict[owner_item] = dict_value + 1
        dict_value += 1

    for parent, dirnames, filenames in os.walk(data_path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename:
                file_path = os.path.join(parent, filename)
                (rate, sig) = wav.read(file_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner = file_path.split('-')[-1][:-4]
                owner_label = len(mfcc_feat) * [owner_dict[owner] - 1]
                y.extend(owner_label)
                for mfcc_vector in mfcc_feat:
                    X.append(mfcc_vector)

    return X, y, owner_dict


def get_owner_dict(data_path):
    dict_value = 0
    owner_list = []
    owner_dict = {}
    for parent, dirnames, filenames in os.walk(data_path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and 'singing' not in filename:
                file_path = os.path.join(parent, filename)
                owner = file_path.split('-')[-1][:-4]
                owner_list.append(owner)
    # print owner_list
    owner_set = set(owner_list)
    for owner_item in owner_set:
        owner_dict[owner_item] = dict_value + 1
        dict_value += 1
    return owner_dict


def process_data_sgd_single(wav_path, owner_dict):
    # process data mfcc
    owner = wav_path.split('-')[-1][:-4]
    X = []
    y = []
    (rate, sig) = wav.read(wav_path)
    mfcc_feat = calcMFCC(sig, rate)
    owner_label = owner_dict[owner] - 1
    for mfcc_vector in mfcc_feat:
        X.append(mfcc_vector)
        y.append(owner_label)
    return X, y


def translate_the_result(name_label=6, owner_dict={'ck': 6}):
    owner_name = ''
    for name, label in owner_dict.items():
        if label == name_label + 1:
            owner_name = name
    return owner_name


def total_sgd_predict_accuracy(path='IUOUI_16000_mono_vad', model='model/sgd.model'):
    accuracy = 0
    total = 0
    process_X, process_y, owner_dict = process_data_sgd(path)
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and'reading'not in filename:
                wav_path = os.path.join(parent, filename)
                owner = wav_path.split('-')[-1][:-4]
                # print wav_path
                total += 1.0
                # process data mfcc
                X = []
                y = []
                (rate, sig) = wav.read(wav_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner_label = owner_dict[owner] - 1
                for mfcc_vector in mfcc_feat:
                    X.append(mfcc_vector)
                    y.append(owner_label)
                # load crf predict
                crf = joblib.load(model)
                predict_names = crf.predict(X)
                print predict_names

                name_singer_speaker = predict_names_vote_one_name(predict_names)
                # print '**********owner_dict:', owner_dict
                # print name_singer_speaker


                if name_singer_speaker == owner_label:
                    accuracy += 1.0
                else:
                    print 'error with ', wav_path
                    print name_singer_speaker
                    # print y
    acc = accuracy / total
    print  'total acc:', acc * 100, '%', ' &recognize right:', accuracy, ',& total:', total

    return acc


def shuffle_X_y(Xx, yy):
    X = []
    y = []
    len_data = len(Xx)
    idx = range(len_data)
    np.random.seed(14)
    np.random.shuffle(idx)
    for item in idx:
        X.append(Xx[item])
        y.append(yy[item])
    return X, y


# X = [[0., 0.], [1., 1.]]
# y = [0, 1]
def main():
    Xx, yy, owner_dict = process_data_sgd('IUOUI_16000_mono_vad')

    X, y = shuffle_X_y(Xx, yy)
    # print y
    sgd = OneVsRestClassifier(SGDClassifier(loss="log", penalty="l2", shuffle=True, n_iter=100, alpha=1.0 / 10000))
    print 'start fit sgd...'
    sgd = sgd.fit(X, y)
    coef = sgd.coef_
    intercept = sgd.intercept_
    # print coef,intercept
    joblib.dump(sgd, 'model/sgd.model')
    print 'saved model sgd'
    predict_label = sgd.predict(X[100])
    print 'the owner is :', translate_the_result(predict_label[0], owner_dict)
    total_sgd_predict_accuracy()
    # total acc: 14.2857142857 %  &recognize right: 8.0 ,& total: 56.0
    return 0

# main()
