import os
import scipy.io.wavfile as wav
import joblib
from sklearn import svm
from sklearn.multiclass import OneVsRestClassifier
from genMFCC import calcMFCC
from testSGD import process_data_sgd, shuffle_X_y, translate_the_result, predict_names_vote_one_name, get_owner_dict


def total_svm_predict_accuracy(path='IUOUI_16000_mono_vad', model='model/svm.model'):
    accuracy = 0
    total = 0
    owner_dict = get_owner_dict('IUOUI_16000_mono_vad')
    for parent, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if '.DS_Store' not in filename and '.doc' not in filename and 'reading' not in filename:
                wav_path = os.path.join(parent, filename)
                owner = wav_path.split('-')[-1][:-4]
                # print wav_path
                total += 1.0
                # process data mfcc
                X = []
                y = []
                (rate, sig) = wav.read(wav_path)
                mfcc_feat = calcMFCC(sig, rate)
                owner_label = owner_dict[owner] - 1
                for mfcc_vector in mfcc_feat:
                    X.append(mfcc_vector)
                    y.append(owner_label)
                # load crf predict
                crf = joblib.load(model)
                predict_names = crf.predict(X)
                print predict_names

                name_singer_speaker = predict_names_vote_one_name(predict_names)

                # print name_singer_speaker
                # print "*********"
                # print owner_label
                if name_singer_speaker == owner_label:
                    accuracy += 1.0
                else:
                    print 'error with ', wav_path
                    print name_singer_speaker
                    # print y
    acc = accuracy / total
    print  'total acc:', acc * 100, '%', ' &recognize right:', accuracy, ',& total:', total

    return acc


# X = [[0, 0], [1, 1]]
# Y = [0, 1]
def main():
    Xx, yy, owner_dict = process_data_sgd('IUOUI_16000_mono_vad')
    X, y = shuffle_X_y(Xx, yy)
    sm = OneVsRestClassifier(svm.SVC(decision_function_shape='ovo'))
    print 'start fit svm model...'
    sm = sm.fit(X, y)
    joblib.dump(sm, 'model/svm.model')
    print 'save svm model over'
    predict_label = sm.predict(X[1])
    print 'the owner is :', translate_the_result(predict_label[0], owner_dict)
    total_svm_predict_accuracy()
    return 0

# main()
